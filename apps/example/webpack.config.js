const nrwlConfig = require("@nrwl/react/plugins/webpack.js")

module.exports = (config, context) => {
  config.node = {
    fs: 'empty',
    net: 'empty',
  }

  config.resolve = {
    fallback: {
        "child_process": false, 
        "process":  false, 
        "fs": false, 
        "util": false, 
        "http": false,
        "https": false,
        "tls": false,
        "net": false,
        "crypto": false, 
        "path": false,
        "os": false, 
        "stream": false,
        "zlib": false
    }
}

  nrwlConfig(config)

  return {
    ...config,
    plugins: [
      ...config.plugins
    ]
  }
}