module.exports = {
  purge: {
    content: [
      './pages/**/*.{js,ts,jsx,tsx}',
      './modules/**/*.{js,ts,jsx,tsx}',
      './node_modules/@dpg-3c-jupiter/**/*.{js,ts,jsx,tsx}',
    ],
    options: {
      keyframes: true,
      variables: true,
    },
  },
  theme: {
    extend: {
      gridTemplateRows: {
        'header-main-footer': 'auto 1fr auto',
      },
      colors: {
        transparent: 'transparent',
        gray: {
          100: '#f7fafc',
          200: '#a0a0bc',
        },
        primary: 'var(--primary)',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/line-clamp')],
};
