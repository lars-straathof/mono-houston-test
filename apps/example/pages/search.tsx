import { GetStaticProps } from 'next';
import { fetchMenu, Menu, getSearchResults } from '@mono-houston/data-access';
import { Layout, Message } from '@mono-houston/components';
import { SearchForm } from '@dpg-3c-jupiter/search';
import {
  ArticleList,
  Button,
  generateDefaultImg,
  generatePictureSources,
  Picture,
} from '@dpg-3c-jupiter/ui';
import { useSearch } from '@mono-houston/hooks';

const Search: React.FC<{ menu?: Menu }> = ({ menu }) => {
  const {
    articles,
    handleLoadMoreClick,
    hasNoResults,
    loading,
    loadingMore,
    loadMoreError,
    searchedText,
    setArticles,
    setLoading,
    setOffset,
    setSearchedText,
    setTotalArticles,
    totalArticles,
  } = useSearch();
  return (
    <Layout title='Search' menu={menu}>
      <div className='container mx-auto my-8 px-4'>
        <main>
          <SearchForm
            onSearch={(input: string) => {
              setLoading(true);
              setSearchedText(input);
              getSearchResults({
                text: input,
                onComplete: ({ error, data }) => {
                  if (error) {
                    <Message message={error.message} error />;
                  }

                  if (data?.results && data?.hits) {
                    setArticles(data?.results);
                    setTotalArticles(data?.hits);
                    setOffset(0);
                  }
                  setLoading(false);
                },
              });
            }}
          />

          {hasNoResults && !loading && <Message message='Geen resultaten' />}

          {loading && !hasNoResults && <Message message='Bezig met zoeken...' />}

          {searchedText.length > 0 && !hasNoResults && !loading && (
            <>
              <Message message={`${articles.length} Resultaten voor: ${searchedText}`} />
              <ArticleList
                data={articles}
                PictureComponent={(article, index) => (
                  <Picture
                    sources={generatePictureSources(article.featuredImage, '128px')}
                    defaultSource={generateDefaultImg(article.featuredImage)}
                    priority={index < 3}
                    classes={{ img: ['object-cover', 'w-32', 'h-24'] }}
                    sizes='128px'
                  />
                )}
              />
              {articles.length < totalArticles && (
                <Button
                  label={loadingMore ? 'Laden...' : 'Laad meer artikelen'}
                  onClick={handleLoadMoreClick}
                />
              )}
              {loadMoreError && <Message message={loadMoreError} error />}
            </>
          )}
        </main>
      </div>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const menu = await fetchMenu('main_menu');
  return {
    props: { menu },
    revalidate: Number(process.env.REQUEST_REVALIDATE_IN_SEC),
  };
};

export default Search;
