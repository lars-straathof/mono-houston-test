import { GetStaticPaths, GetStaticProps } from 'next';
import Error from 'next/error';
import {
  fetchArticles,
  fetchArticleDetail,
  ArticleDetails,
  fetchMenu,
  Menu,
} from '@mono-houston/data-access';
import { Layout } from '@mono-houston/components';
import { ArticleCategory, ArticleDetail } from '@dpg-3c-jupiter/ui';

interface Props {
  article: ArticleDetails;
  menu: Menu;
}

const ArticlePage: React.FC<Props> = ({ article, menu }) => {
  if (!article) {
    return <Error statusCode={404} />;
  }

  const {
    articleMetaData: {
      navigation: { seoKey },
    },
  } = article;

  return (
    <Layout title={article.articleMetaData.title} menu={menu}>
      {seoKey && (
        <div className="container mx-auto my-4 px-4">
          <ArticleCategory slug={seoKey} />
        </div>
      )}
      <ArticleDetail data={article} />
    </Layout>
  );
};

export default ArticlePage;

export const getStaticProps: GetStaticProps = async (context) => {
  const article = await fetchArticleDetail(context.params.articleId.toString());
  const menu = await fetchMenu('main_menu');

  if (!article) {
    return {
      notFound: true,
    };
  }

  return {
    props: { article, menu },
    revalidate: Number(process.env.REQUEST_REVALIDATE_IN_SEC),
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const articles = await fetchArticles();

  const paths =
    articles &&
    articles.results.map((article) => ({
      params: { articleId: article.id },
    }));

  return {
    paths,
    fallback: 'blocking',
  };
};
