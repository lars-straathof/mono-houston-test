import { GetStaticProps } from 'next';
import { Layout } from '@mono-houston/components';
import { fetchMenu, Menu } from '@mono-houston/data-access';

const Error404: React.FC<{ menu?: Menu }> = ({ menu }) => {
  return (
    <Layout title='404' menu={menu}>
      <div className='container mx-auto my-8 px-4'>
        <h1 className='text-4xl'>404 - Page not found</h1>
      </div>
    </Layout>
  );
};
export const getStaticProps: GetStaticProps = async () => {
  const menu = await fetchMenu('main_menu');
  return {
    props: { menu },
    revalidate: Number(process.env.REQUEST_REVALIDATE_IN_SEC),
  };
};

export default Error404;
