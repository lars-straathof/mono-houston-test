import '../styles/global.css';
import '../styles/dnr_minimal.css';

import { Provider } from 'next-auth/client';
import { ThemeProvider } from 'react-jss';

import { lightTheme } from '@mono-houston/config';

import type { AppProps } from 'next/app';

export default function App({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <Provider session={pageProps.session}>
      <ThemeProvider theme={lightTheme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </Provider>
  );
}
