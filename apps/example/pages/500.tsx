import { GetStaticProps } from 'next';
import { Layout } from '@mono-houston/components';
import { fetchMenu, Menu } from '@mono-houston/data-access';

const Error500: React.FC<{ menu?: Menu }> = ({ menu }) => {
  return (
    <Layout title='500' menu={menu}>
      <div className='container mx-auto my-8 px-4'>
        <h1 className='text-4xl'>500 - Server-side error occurred</h1>
      </div>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const menu = await fetchMenu('main_menu');
  return {
    props: { menu },
    revalidate: Number(process.env.REQUEST_REVALIDATE_IN_SEC),
  };
};

export default Error500;
