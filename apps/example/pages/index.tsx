import { GetStaticProps } from 'next';
import Link from 'next/link';
// import { generateSitemap } from '@mono-houston/utils';

import {
  fetchArticles,
  fetchCategories,
  fetchMenu,
  Menu,
  Block,
  PageWithBlocks,
  fetchPageBlock,
  fetchPageWithBlocks,
} from '@mono-houston/data-access';
import {
  PageBlocks,
  SingleBlock,
  Layout,
  ArticleListPicture,
} from '@mono-houston/components';

import { Article } from '@dpg-3c-jupiter/article-list-api';
import { Category } from '@dpg-3c-jupiter/site-management-api';
import { ArticleList } from '@dpg-3c-jupiter/ui';

interface HomeProps {
  featuredArticles?: Article[];
  latestArticles?: Article[];
  categories?: Category[];
  pageBlocks?: PageWithBlocks;
  pageImageBlock?: Block;
  menu?: Menu;
}

const Home: React.FC<HomeProps> = ({
  featuredArticles,
  latestArticles,
  categories,
  pageBlocks,
  pageImageBlock,
  menu,
}) => (
  <Layout title="Home" menu={menu}>
    <div className="container mx-auto my-8 px-4">
      <main>
        {categories && (
          <>
            <h3 className="font-bold text-3xl text-gray-400 my-4">
              Categories
            </h3>
            {categories.map((category) => (
              <div key={category.slug}>
                <Link href={`/category/${category.slug}`}>
                  <a className="font-bold text-lg block">{category.label}</a>
                </Link>
              </div>
            ))}
          </>
        )}

        {featuredArticles && (
          <ArticleList
            data={featuredArticles}
            PictureComponent={ArticleListPicture}
          />
        )}

        {latestArticles && (
          <>
            <h3 className="font-bold text-3xl text-gray-400 pt-8 my-4">
              Nieuw
            </h3>
            <ArticleList
              data={latestArticles}
              PictureComponent={ArticleListPicture}
            />
          </>
        )}

        <div className="pb-6">
          {pageImageBlock && <SingleBlock block={pageImageBlock} />}
        </div>

        {pageBlocks && <PageBlocks page={pageBlocks} />}
      </main>
    </div>
  </Layout>
);

export default Home;

export const getStaticProps: GetStaticProps<HomeProps> = async () => {
  const featuredArticles = await fetchArticles({ isFeatured: true, limit: 3 });
  const latestArticles = await fetchArticles();
  const categories = await fetchCategories();
  const pageBlocks = await fetchPageWithBlocks('home');
  const pageImageBlock = await fetchPageBlock('home', 'i70sk9qeJ8v2I0gr');
  const menu = await fetchMenu('main_menu');

  // generateSitemap();

  return {
    props: {
      featuredArticles: featuredArticles.results,
      latestArticles: latestArticles.results,
      categories,
      pageBlocks,
      pageImageBlock,
      menu,
    },
    revalidate: Number(process.env.REQUEST_REVALIDATE_IN_SEC),
  };
};
