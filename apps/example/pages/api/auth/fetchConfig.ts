import type { NextApiRequest, NextApiResponse } from 'next';
import fs from 'fs';
import path from 'path';
import util from 'util';

import { withSentry } from '@sentry/nextjs';

const writeFile = util.promisify(fs.writeFile);
const AUTH_CONFIG_PATH = path.join(__dirname, '../../../../../config/auth.json');

const fetchConfig = (_req: NextApiRequest, res: NextApiResponse): void => {
  try {
    fetch(process.env.OPENID_CONFIG_URL).then(async (response) => {
      const data = await response.json();

      await writeFile(AUTH_CONFIG_PATH, JSON.stringify(data));

      res.status(200).json({ status: true });
    });
  } catch (err) {
    res.status(500);
  }
};

export default withSentry(fetchConfig);
