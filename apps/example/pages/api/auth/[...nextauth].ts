import NextAuth from 'next-auth';

import {authConfig} from '@mono-houston/config';

import { signIn, redirect, session, jwt, profile } from '@mono-houston/auth';

export default NextAuth({
  debug: process.env.DEBUG === 'true',
  providers: [
    {
      id: 'pip',
      name: 'PIP',
      type: 'oauth',
      version: '2.0',
      scope: 'openid profile email',
      params: { grant_type: 'authorization_code' },
      accessTokenUrl: authConfig.token_endpoint,
      requestTokenUrl: authConfig.authorization_endpoint,
      authorizationUrl: authConfig.authorization_endpoint + '?response_type=code',
      profileUrl: authConfig.userinfo_endpoint,
      // @ts-expect-error test, to ignore type errors for now
      profile,
      clientId: process.env.NEXT_PUBLIC_OAUTH_CLIENT_ID,
      clientSecret: process.env.PIP_OAUTH_CLIENT_SECRET,
      headers: {
        'x-pipoidc-bot': process.env.PIP_OIDC_BOT_KEY,
        Authorization: `Basic ${Buffer.from(
          `${process.env.NEXT_PUBLIC_OAUTH_CLIENT_ID}:${process.env.PIP_OAUTH_CLIENT_SECRET}`,
          'ascii'
        ).toString('base64')}`,
      },
    },
  ],
  callbacks: {
    // @ts-expect-error test, to ignore type errors for now
    signIn,
    // @ts-expect-error test, to ignore type errors for now
    redirect,
    // @ts-expect-error test, to ignore type errors for now
    session,
    // @ts-expect-error test, to ignore type errors for now
    jwt,
  },
  session: {
    // Use JSON Web Tokens for session instead of database sessions.
    // This option can be used with or without a database for users/accounts.
    // Note: `jwt` is automatically set to `true` if no database is specified.
    jwt: true,

    // Seconds - How long until an idle session expires and is no longer valid.
    maxAge: 30 * 24 * 60 * 60, // 30 days

    // Seconds - Throttle how frequently to write to database to extend a session.
    // Use it to limit write operations. Set to 0 to always update the database.
    // Note: This option is ignored if using JSON Web Tokens
    updateAge: 24 * 60 * 60, // 24 hours
  },
});
