import { fetchCategoryArticles } from '@mono-houston/data-access';
import { withSentry } from '@sentry/nextjs';

import type { NextApiRequest, NextApiResponse } from 'next';

const handler = async (req: NextApiRequest, res: NextApiResponse): Promise<void> => {
  const category: string | null = req.query.params[0] || undefined;
  const offset: number | null = (req.query.params[1] as unknown as number) || 0;
  const articleList = await fetchCategoryArticles({
    category,
    offset: offset,
  });
  res.end(JSON.stringify(articleList));
};

export default withSentry(handler);
