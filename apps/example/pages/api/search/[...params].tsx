import { fetchArticles } from '@mono-houston/data-access';
import { withSentry } from '@sentry/nextjs';

import type { NextApiRequest, NextApiResponse } from 'next';

const handler = async (req: NextApiRequest, res: NextApiResponse): Promise<void> => {
  const searchQuery = req.query.params[0];
  const offset = req.query.params[1] || 0;
  const articleList = await fetchArticles({
    searchQuery: searchQuery as string,
    offset: offset as number,
  });
  res.end(JSON.stringify(articleList));
};

export default withSentry(handler);
