import { GetStaticPaths, GetStaticProps } from 'next';
import { Layout, PageBlocks } from '@mono-houston/components';
import { fetchPages, fetchPageWithBlocks, PageWithBlocks } from '@mono-houston/data-access';

interface Props {
  page: PageWithBlocks;
}

const StaticPage: React.FC<Props> = ({ page }) => (
  <Layout title={page.title}>
    <div className='container mx-auto my-8 px-4'>
      <main>
        <h3 className='font-bold text-3xl text-gray-400 my-4'>{page.title}</h3>

        {page.content && <PageBlocks page={page} />}
      </main>
    </div>
  </Layout>
);

export default StaticPage;

export const getStaticProps: GetStaticProps<Props> = async (context) => {
  const page = await fetchPageWithBlocks(context.params.pageSlug.toString());

  return {
    props: {
      page,
    },
    revalidate: Number(process.env.REQUEST_REVALIDATE_IN_SEC),
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const fetchedPages = await fetchPages();

  const paths = fetchedPages.map((page) => ({
    params: { pageSlug: page.slug },
  }));

  return {
    paths,
    fallback: false,
  };
};
