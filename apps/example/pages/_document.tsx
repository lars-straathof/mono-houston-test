import Document, {
  DocumentContext,
  DocumentInitialProps,
  Head,
  Html,
  Main,
  NextScript,
} from 'next/document';
import { ReactElement } from 'react';
import { createGenerateId, JssProvider, SheetsRegistry } from 'react-jss';

import { Consent } from '@dpg-3c-jupiter/consent';
import { GtmNoScript } from '@dpg-3c-jupiter/common';

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext): Promise<DocumentInitialProps> {
    const registry = new SheetsRegistry();
    const generateId = createGenerateId();
    const originalRenderPage = ctx.renderPage;

    ctx.renderPage = () =>
      originalRenderPage({
        // eslint-disable-next-line react/display-name
        enhanceApp: (App) => (props) =>
          (
            <JssProvider registry={registry} generateId={generateId}>
              <App {...props} />
            </JssProvider>
          ),
      });

    const initialProps = await Document.getInitialProps(ctx);

    return {
      ...initialProps,
      styles: (
        <>
          {initialProps.styles}
          <style id='server-side-styles'>{registry.toString()}</style>
        </>
      ),
    };
  }

  render(): ReactElement {
    return (
      <Html lang='nl'>
        <Head />
        <Consent
          cmpProperties={{
            baseUrl: process.env.SOURCEPOINT_BASEURL,
            cmpCname: process.env.SOURCEPOINT_CMP_NAME,
            language: 'nl',
          }}
        />
        <body className='bg-white text-black dark:bg-gray-900 dark:text-gray-200'>
          <GtmNoScript gtmId={process.env.NEXT_PUBLIC_GTM_ID} />
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
