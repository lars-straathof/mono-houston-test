import { GetStaticPaths, GetStaticProps } from 'next';
import { Layout, Message } from '@mono-houston/components';
import {
  fetchCategoryArticles,
  fetchCategories,
  fetchCategory,
  fetchMenu,
  Menu,
} from '@mono-houston/data-access';
import { useCategory } from '@mono-houston/hooks';
import { ArticleList as ALInterface } from '@dpg-3c-jupiter/article-list-api';
import {
  ArticleList,
  Button,
  generateDefaultImg,
  generatePictureSources,
  Picture,
} from '@dpg-3c-jupiter/ui';
import { Category } from '@dpg-3c-jupiter/site-management-api';

interface Props {
  fetchedCategoryArticleList: ALInterface;
  fetchedCategory: Category;
  slug: string;
  menu: Menu;
}

const CategoryPage: React.FC<Props> = ({
  fetchedCategoryArticleList,
  fetchedCategory,
  slug,
  menu,
}) => {
  const {
    articles,
    handleLoadMoreClick,
    loadingMore,
    loadMoreError,
    totalArticles,
  } = useCategory(fetchedCategoryArticleList, slug);

  return (
    <Layout title={fetchedCategory && fetchedCategory.label} menu={menu}>
      <div className="container mx-auto my-8 px-4">
        <main>
          <h3 className="font-bold text-3xl text-gray-400 my-4">
            {fetchedCategory && fetchedCategory.label}
          </h3>
          <ArticleList
            data={articles}
            PictureComponent={(article, index) => (
              <Picture
                sources={generatePictureSources(article.featuredImage, '128px')}
                defaultSource={generateDefaultImg(article.featuredImage)}
                priority={index < 3}
                classes={{ img: ['object-cover', 'w-32', 'h-24'] }}
                sizes="128px"
              />
            )}
          />
          {articles && articles.length < totalArticles && (
            <Button
              label={loadingMore ? 'Laden...' : 'Laad meer artikelen'}
              onClick={handleLoadMoreClick}
            />
          )}
          {loadMoreError && <Message message={loadMoreError} error />}
        </main>
      </div>
    </Layout>
  );
};

export default CategoryPage;

export const getStaticProps: GetStaticProps<Props> = async (context) => {
  const fetchedCategoryArticleList = await fetchCategoryArticles({
    category: context.params.categorySlug.toString(),
  });

  const fetchedCategory = await fetchCategory(
    context.params.categorySlug.toString()
  );
  const menu = await fetchMenu('main_menu');

  return {
    props: {
      fetchedCategoryArticleList,
      fetchedCategory,
      slug: context.params.categorySlug.toString(),
      menu,
    },
    revalidate: Number(process.env.REQUEST_REVALIDATE_IN_SEC),
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const fetchedCategories = await fetchCategories();

  const paths = fetchedCategories.map((category) => ({
    params: { categorySlug: category.slug },
  }));

  return {
    paths,
    fallback: true,
  };
};
