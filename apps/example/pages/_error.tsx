import { NextPage } from 'next';

interface CustomErrorProps {
  statusCode?: number;
  title?: string;
  message?: string;
}

const Error: NextPage<CustomErrorProps> = ({ statusCode, title, message }) => (
  <div>
    {title && <h1>{title}</h1>}
    <p>
      {statusCode ? `An error ${statusCode} occurred on server` : 'An error occurred on client'}
    </p>
    {message && <p>{message}</p>}
  </div>
);

Error.getInitialProps = async ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default Error;
