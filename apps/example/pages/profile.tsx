import { GetStaticProps } from 'next';
import { useSession } from 'next-auth/client';
import { ExternalLinkIcon } from '@heroicons/react/solid';
import { fetchMenu, Menu } from '@mono-houston/data-access';

import { Layout } from '@mono-houston/components';
import { signOut } from '@mono-houston/auth';

const Profile: React.FC<{ menu?: Menu }> = ({ menu }) => {
  const [session] = useSession();

  return (
    <Layout title='Profile' menu={menu}>
      <div className='container mx-auto my-8 px-4'>
        <main>
          <h2 className='text-2xl'>Mijn Jupiter</h2>
          {session && (
            <>
              <div className='mt-10'>
                <p className='font-bold'>
                  <a href='#' onClick={() => signOut()}>
                    Uitloggen
                  </a>
                </p>
                <p>{session.user.email}</p>
              </div>
              <div className='mt-10'>
                <p className='font-bold'>
                  <a
                    href='#'
                    onClick={() => {
                      console.log('MyAccount not implemented yet');
                    }}
                  >
                    Mijn account
                  </a>
                </p>
                <div className='flex'>
                  <p className='pr-10'>
                    Beheer van uw account, abonnementen en e-mail nieuwsbrieven
                  </p>
                  <ExternalLinkIcon width={35} />
                </div>
              </div>
            </>
          )}

          {!session && <div>U bent niet ingelogd.</div>}
        </main>
      </div>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const menu = await fetchMenu('main_menu');
  return {
    props: { menu },
    revalidate: Number(process.env.REQUEST_REVALIDATE_IN_SEC),
  };
};

export default Profile;
