import { Session, User } from 'next-auth';
import { JWT } from 'next-auth/jwt';

export const session = async (session: Session, userOrToken: JWT | User): Promise<Session> => {
  if (process.env.DEBUG) {
    console.log('next-auth/session callback', session, userOrToken);
  }

  // Add property to session, like an access_token from a provider.
  session.accessToken = userOrToken.accessToken;

  return session;
};
