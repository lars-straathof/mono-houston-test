import { Account, Profile, User } from 'next-auth';

export const signIn = async (
  user: User,
  account: Account,
  profile: Profile
): Promise<string | boolean> => {
  if (process.env.DEBUG) {
    console.log('next-auth/signIn callback', user, account, profile);
  }

  return true;
};
