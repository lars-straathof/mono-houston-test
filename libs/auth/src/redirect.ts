export const redirect = async (url: string, baseUrl: string): Promise<string> => {
  if (process.env.DEBUG) {
    console.log('next-auth/redirect callback', url, baseUrl);
  }

  return url;
};
