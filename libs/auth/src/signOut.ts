import { authConfig } from '@mono-houston/config';
import { signOut as NextSignOut } from 'next-auth/client';

const { issuer } = authConfig;
const PIP_LOGOUT_URL = `${issuer}/logout?client_id=${process.env.NEXT_PUBLIC_OAUTH_CLIENT_ID}&post_logout_redirect_uri=${process.env.NEXT_PUBLIC_BASE_URL}`;

export const signOut = (): Promise<void> => {
  if (process.env.DEBUG) {
    console.log('next-auth/signOut callback', PIP_LOGOUT_URL);
  }
  return NextSignOut({ callbackUrl: PIP_LOGOUT_URL });
};
