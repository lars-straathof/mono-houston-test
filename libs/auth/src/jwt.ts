import { Account, Profile, User } from 'next-auth';
import { JWT } from 'next-auth/jwt';

export const jwt = async (
  token: JWT,
  user: User,
  account: Account,
  profile: Profile,
  isNewUser: boolean
): Promise<JWT> => {
  if (process.env.DEBUG) {
    console.log('next-auth/jwt callback', token, user, account, profile, isNewUser);
  }
  // Add access_token to the token right after signin
  if (account?.accessToken) {
    token.accessToken = account.accessToken;
  }
  // Add email to jwt token
  if (profile?.email) {
    token.email = profile.email;
  }
  return token;
};
