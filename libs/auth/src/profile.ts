import { Profile, TokenSet, User } from 'next-auth';

export const profile = async (
  profile: Profile,
  tokens: TokenSet
): Promise<
  User & {
    id: string;
  }
> => {
  if (process.env.DEBUG) {
    console.log('next-auth/profile callback', profile, tokens);
  }

  return {
    id: profile.sub,
  };
};
