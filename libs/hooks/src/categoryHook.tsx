import { useEffect, useState } from 'react';
import { Article, ArticleList } from '@dpg-3c-jupiter/article-list-api';
import { getCategoryResults } from '@mono-houston/data-access';

const ARTICLES_LISTING_LIMIT = process.env.NEXT_PUBLIC_ARTICLES_LISTING_LIMIT;

interface UseCategoryReturn {
  articles: Article[];
  handleLoadMoreClick: () => void;
  loadingMore: boolean;
  loadMoreError: string;
  totalArticles: number;
}

export const useCategory = (
  fetchedCategoryArticleList: ArticleList,
  slug: string
): UseCategoryReturn => {
  const [articles, setArticles] = useState<Article[] | undefined>(
    fetchedCategoryArticleList?.results
  );
  const [totalArticles, setTotalArticles] = useState<number>(fetchedCategoryArticleList?.hits);
  const [loadingMore, setLoadingMore] = useState<boolean>(false);
  const [loadMoreError, setLoadMoreError] = useState<string>('');
  const [offset, setOffset] = useState<number>(0);

  const handleLoadMoreClick = () => setLoadingMore(true);
  useEffect(() => {
    if (loadingMore) {
      const newOffset = offset + Number(ARTICLES_LISTING_LIMIT);
      setOffset(newOffset);
      getCategoryResults({
        text: slug,
        offset: newOffset,
        onComplete: ({ error, data }) => {
          if (error) {
            setLoadMoreError(error.message);
          }

          if (data?.results && data?.hits) {
            const newArticleList = [...articles, ...data.results];
            setArticles(newArticleList);
            setTotalArticles(data.hits);
          }

          setLoadingMore(false);
        },
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loadingMore]);

  return {
    articles,
    handleLoadMoreClick,
    loadingMore,
    loadMoreError,
    totalArticles,
  };
};
