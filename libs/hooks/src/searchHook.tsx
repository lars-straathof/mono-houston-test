import { Dispatch, SetStateAction, useEffect, useState } from 'react';
import { getSearchResults } from '@mono-houston/data-access';
import { Article } from '@dpg-3c-jupiter/article-list-api';

const ARTICLES_LISTING_LIMIT = process.env.NEXT_PUBLIC_ARTICLES_LISTING_LIMIT;

interface useSearchReturn {
  articles: Article[];
  handleLoadMoreClick: () => void;
  hasNoResults: boolean;
  loading: boolean;
  loadingMore: boolean;
  loadMoreError: string;
  searchedText: string;
  setArticles: Dispatch<SetStateAction<Article[]>>;
  setLoading: Dispatch<SetStateAction<boolean>>;
  setOffset: Dispatch<SetStateAction<number>>;
  setSearchedText: Dispatch<SetStateAction<string>>;
  setTotalArticles: Dispatch<SetStateAction<number>>;
  totalArticles: number;
}

export const useSearch = (): useSearchReturn => {
  const [articles, setArticles] = useState<Article[] | undefined>();
  const [loading, setLoading] = useState(false);
  const [loadingMore, setLoadingMore] = useState(false);
  const [loadMoreError, setLoadMoreError] = useState<string>('');
  const [offset, setOffset] = useState<number>(0);
  const [totalArticles, setTotalArticles] = useState<number>(0);
  const [searchedText, setSearchedText] = useState<string>('');
  const hasNoResults = searchedText.length > 0 && (!articles || articles.length === 0);

  const handleLoadMoreClick = () => setLoadingMore(true);

  useEffect(() => {
    if (loadingMore) {
      const newOffset = offset + Number(ARTICLES_LISTING_LIMIT);
      setOffset(newOffset);
      getSearchResults({
        text: searchedText,
        offset: newOffset,
        onComplete: ({ error, data }) => {
          if (error) {
            setLoadMoreError(error.message);
          }

          if (data?.results && data?.hits) {
            const newData = [...articles, ...data.results];
            setArticles(newData);
            setTotalArticles(data.hits);
          }

          setLoadingMore(false);
        },
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loadingMore]);

  return {
    articles,
    handleLoadMoreClick,
    hasNoResults,
    loading,
    loadingMore,
    loadMoreError,
    searchedText,
    setArticles,
    setLoading,
    setOffset,
    setSearchedText,
    setTotalArticles,
    totalArticles,
  };
};
