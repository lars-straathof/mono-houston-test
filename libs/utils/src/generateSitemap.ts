import fs from 'fs';
import path from 'path';
import { fetchArticles, fetchCategories } from '@mono-houston/data-access';
import { Article } from '@dpg-3c-jupiter/article-list-api';
import { Category } from '@dpg-3c-jupiter/site-management-api';

type SitemapEntry = {
  url: string;
  lastModified: string;
};

const IGNORE_PATHS = [
  '_app.tsx',
  '_document.tsx',
  '_error.tsx',
  '404.tsx',
  '500.tsx',
  'api',
  'index.tsx',
  'profile.tsx',
  'article',
  'category',
];

const EXTENSIONS = ['.ts', '.tsx', '.js', '.jsx'];

const STATIC_LAST_MODIFIED = new Date().toISOString(); // this will be the build date;

// TODO: Generate index sitemap and multiple sitemaps when there are many articles
export const generateSitemap: () => Promise<void> = async () => {
  const pagesDirectory = path.resolve('./pages');

  const allPageFiles = fs.readdirSync(pagesDirectory);

  let sitemapStaticPages: SitemapEntry[] = [];
  let fetchedArticlesMapped: SitemapEntry[] = [];
  let fetchedFeaturedArticlesMapped: SitemapEntry[] = [];
  let fetchedCategoriesMapped: SitemapEntry[] = [];

  if (allPageFiles && allPageFiles.length) {
    sitemapStaticPages = allPageFiles
      .filter((staticPage) => !IGNORE_PATHS.includes(staticPage))
      .map((staticPagePath) => ({
        url: `${process.env.NEXT_PUBLIC_BASE_URL}/${staticPagePath}`.replace(
          new RegExp(`/${EXTENSIONS.join('|')}/g`),
          ''
        ),
        lastModified: STATIC_LAST_MODIFIED,
      }));
  }

  const fetchedFeaturedArticles = await fetchArticles({
    limit: Number(process.env.SITEMAP_ARTICLE_LIMIT),
    isFeatured: true,
  });

  if (fetchedFeaturedArticles?.results && fetchedFeaturedArticles.results.length) {
    fetchedFeaturedArticlesMapped = fetchedFeaturedArticles.results.map((article: Article) => ({
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/article/${article.id}`,
      lastModified: new Date(article.createdAt).toISOString(), // we need modifiedAt
    }));
  }

  const fetchedArticles = await fetchArticles({
    limit: Number(process.env.SITEMAP_ARTICLE_LIMIT),
  });

  if (fetchedArticles?.results && fetchedArticles.results.length) {
    fetchedArticlesMapped = fetchedArticles.results.map((article: Article) => ({
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/article/${article.id}`,
      lastModified: new Date(article.createdAt).toISOString(), // we need modifiedAt
    }));
  }

  const fetchedCategories = await fetchCategories({
    limit: Number(process.env.SITEMAP_CATEGORY_LIMIT),
  });

  if (fetchedCategories && fetchedCategories.length) {
    fetchedCategoriesMapped = fetchedCategories.map((category: Category) => ({
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/category/${category.slug}`,
      lastModified: new Date().toISOString(), // what do we use here?, just like articles we need modifiedAt
    }));
  }

  const sitemapEntries: SitemapEntry[] = [
    ...sitemapStaticPages,
    ...fetchedFeaturedArticlesMapped,
    ...fetchedArticlesMapped,
    ...fetchedCategoriesMapped,
  ];

  // TODO: set <priority>1.0</priority> and other lvl based on type of article?
  // TODO: check if changefreq: monthly is good, or should be daily?
  if (sitemapEntries && sitemapEntries.length) {
    const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
      <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        ${sitemapEntries
          .map((entry: SitemapEntry) => {
            return `
              <url>
                <loc>${entry.url}</loc>
                <lastmod>${entry.lastModified}</lastmod>
                <changefreq>monthly</changefreq>
              </url>
            `;
          })
          .join('')}
      </urlset>
    `;
    const sitemapFileLocation = path.resolve('./public/sitemap.xml');
    fs.writeFileSync(sitemapFileLocation, sitemap);
  }
};
