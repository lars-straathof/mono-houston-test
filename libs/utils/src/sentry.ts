import * as Sentry from '@sentry/nextjs';
import { CaptureContext } from '@sentry/types';

export const exceptionHandler = (
  receivedError: Error | string,
  captureContext?: CaptureContext
): void => {
  const error = typeof receivedError === 'string' ? new Error(receivedError) : receivedError;

  if (process.env.USE_SENTRY === 'true') {
    Sentry.captureException(error, captureContext);
  }

  if (process.env.DEBUG === 'true') {
    console.error(`${error.name} - ${error.message}`);
  }

};
