// eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
export const parseApiResponse = (data: any): any => {
  if (!data) return null;
  // openapi generator returns JSON<DataType> | JSON<Array<DataType>>
  // so we need to stringify and parse the data
  return JSON.parse(JSON.stringify(data));
};
