import { parseApiResponse, exceptionHandler } from '@mono-houston/utils';
import { DefaultApi, Page } from '@dpg-3c-jupiter/site-management-api';

export const fetchPages = async (): Promise<Page[]> => {
  const api = new DefaultApi(process.env.SITE_MANAGEMENT_URL);

  return await api
    .getPages()
    .then(({ body }) => parseApiResponse(body))
    .catch(exceptionHandler);
};

export const fetchPage = async (slug: string): Promise<Page> => {
  const api = new DefaultApi(process.env.SITE_MANAGEMENT_URL);

  return await api
    .getPage(slug)
    .then(({ body }) => parseApiResponse(body))
    .catch(exceptionHandler);
};
