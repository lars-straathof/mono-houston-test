import getConfig from 'next/config';
import { exceptionHandler } from '@mono-houston/utils';

interface ArticleMetaData {
  id: string;
  title: string;
  navigation: {
    seoKey: string;
  };
}

export interface ArticleDetails {
  html: string;
  js: string[];
  css: string[];
  articleMetaData: ArticleMetaData;
  metadata: ArticleMetaData;
}


const {
  serverRuntimeConfig: { DNR_ARTICLEDETAIL_PASSWORD },
} = getConfig();

export const articleDetailFetcher = (url: string): Promise<ArticleDetails> =>
  fetch(url, {
    headers: {
      Authorization: `Basic ${Buffer.from(
        process.env.DNR_ARTICLEDETAIL_USER + ':' + DNR_ARTICLEDETAIL_PASSWORD
      ).toString('base64')}`,
    },
  })
    .then((response) => {
      if (response.status !== 200) {
        // @NOTE: should we log http errors to sentry?
        exceptionHandler(`${url} ${response.statusText}`);
        return null;
      }

      return response.json();
    })
    .catch(exceptionHandler);

export const fetchArticleDetail = async (articleId: string): Promise<ArticleDetails> => {
  return await articleDetailFetcher(`${process.env.DNR_ARTICLEDETAIL_URL}${articleId}`);
};
