import { parseApiResponse, exceptionHandler } from '@mono-houston/utils';
import { DefaultApi, Category } from '@dpg-3c-jupiter/site-management-api';

interface CategoryParameters {
  limit?: number;
}

export const fetchCategories = async ({
  limit = Number(process.env.CATEGORY_LISTING_LIMIT),
}: CategoryParameters = {}): Promise<Category[]> => {
  const api = new DefaultApi(process.env.SITE_MANAGEMENT_URL);

  if (process.env.DEBUG) console.warn('TODO: add limit arg for fetchCategories', limit);

  return await api
    .getCategories()
    .then(({ body }) => parseApiResponse(body))
    .catch(exceptionHandler);
};

export const fetchCategory = async (categorySlug: string): Promise<Category> => {
  const api = new DefaultApi(process.env.SITE_MANAGEMENT_URL);

  return await api
    .getCategory(categorySlug)
    .then(({ body }) => parseApiResponse(body))
    .catch(exceptionHandler);
};
