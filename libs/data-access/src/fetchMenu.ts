import { parseApiResponse, exceptionHandler } from '@mono-houston/utils';
import { DefaultApi } from '@dpg-3c-jupiter/site-management-api';

export interface Menu {
  id: number;
  name: string;
  slug: string;
  locale: string;
  menuItems: MenuItem[];
}
export interface MenuItem {
  id: number;
  name: string;
  type: 'static-url' | 'text';
  value: string;
  target: '_self' | '_blank';
  enabled: boolean;
  children: MenuItem[];
}

export const fetchMenu = async (slug: string): Promise<Menu> => {
  const api = new DefaultApi(process.env.SITE_MANAGEMENT_URL);

  return await api
    .getMenu(slug)
    .then(({ body }) => parseApiResponse(body))
    .catch(exceptionHandler);
};
