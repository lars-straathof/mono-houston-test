import { exceptionHandler } from '@mono-houston/utils';
import { ArticleList } from '@dpg-3c-jupiter/article-list-api';

interface SearchResponse {
  data?: ArticleList | undefined;
  error?: Error | null;
}

interface GetCategoryResultsProps {
  text: string;
  offset?: number;
  onComplete: ({ error, data }: SearchResponse) => void;
}

export const getCategoryResults = ({
  text,
  offset = 0,
  onComplete,
}: GetCategoryResultsProps): Promise<void> =>
  fetch(`/api/category/${encodeURIComponent(text)}/${offset}`)
    .then((r) => r.json())
    .then((data) =>
      onComplete({
        data,
      })
    )
    .catch((error) => {
      exceptionHandler(error);
      onComplete({ error: typeof error === 'string' ? new Error(error) : error });
    });
