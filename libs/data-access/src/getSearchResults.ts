import { exceptionHandler } from '@mono-houston/utils';
import { ArticleList } from '@dpg-3c-jupiter/article-list-api';

interface SearchResponse {
  data?: ArticleList | undefined;
  error?: Error | null;
}

interface GetSearchResultsProps {
  text: string;
  offset?: number;
  onComplete: ({ error, data }: SearchResponse) => void;
}

export const getSearchResults = ({
  text,
  offset = 0,
  onComplete,
}: GetSearchResultsProps): Promise<void> =>
  fetch(`/api/search/${encodeURIComponent(text)}/${offset}`)
    .then(async (r) => {
      const results = await r.json();
      onComplete({
        data: results,
      });
    })
    .catch((error) => {
      exceptionHandler(error);
      onComplete({ error: typeof error === 'string' ? new Error(error) : error });
    });
