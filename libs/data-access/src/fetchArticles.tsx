import { parseApiResponse, exceptionHandler } from '@mono-houston/utils';
import { ArticleList, DefaultApi } from '@dpg-3c-jupiter/article-list-api';

interface fetchArticleOptions {
  searchQuery?: string;
  limit?: number;
  offset?: number;
  isFeatured?: boolean;
}

export const fetchArticles = async ({
  searchQuery = null,
  limit = Number(process.env.NEXT_PUBLIC_ARTICLES_LISTING_LIMIT),
  offset = 0,
  isFeatured = null,
}: fetchArticleOptions = {}): Promise<ArticleList> => {
  const api = new DefaultApi(process.env.ARTICLE_LIST_URL);

  return await api
    .getArticles(process.env.TENANT, searchQuery, isFeatured, offset, limit)
    .then(({ body }) => parseApiResponse(body))
    .catch(exceptionHandler);
};
