import { parseApiResponse, exceptionHandler } from '@mono-houston/utils';
import { ArticleList, DefaultApi } from '@dpg-3c-jupiter/article-list-api';

interface CategoryParameters {
  category: string;
  limit?: number;
  offset?: number;
}

export const fetchCategoryArticles = async ({
  category,
  limit,
  offset = 0,
}: CategoryParameters): Promise<ArticleList> => {
  const api = new DefaultApi(process.env.ARTICLE_LIST_URL);

  return await api
    .getCategoryArticles(
      process.env.TENANT,
      category,
      offset,
      limit ?? Number(process.env.NEXT_PUBLIC_ARTICLES_LISTING_LIMIT)
    )
    .then((body) => parseApiResponse(body.body))
    .catch(exceptionHandler);
};
