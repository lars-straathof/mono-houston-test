import { Article } from '@dpg-3c-jupiter/article-list-api';
import { Attributes, Page, PageContent } from '@dpg-3c-jupiter/site-management-api';

import { BlockType } from '@mono-houston/components';
import { fetchCategoryArticles, fetchPage } from '@mono-houston/data-access';

// @TODO: implement (block) interfaces from API
// https://atlassian.dpgmedia.net/jira/browse/HOUS-467

interface BlockAttributes extends Attributes {
  src?: string;
  alt?: string;
  width?: number;
  height?: number;
  title?: string;
  text?: string;
}

interface Image {
  src?: string;
  alt?: string;
  width?: number;
  height?: number;
  title?: string;
}

interface Content {
  text?: string;
}

export interface Block extends Omit<PageContent, 'layout'> {
  // @TODO: figure out why interfaces from API are throwing errors in client
  // one theory is API is exporting classes instead of types/interfaces
  // overriding layout here to fix this for now.
  layout: BlockType;
  data?: Article[] | Image | Content;
  attributes: BlockAttributes;
}

export interface PageWithBlocks extends Omit<Page, 'content'> {
  content: Block[];
}

// @TODO: implement API and interfaces
// https://atlassian.dpgmedia.net/jira/browse/HOUS-467
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const fetchPageBlock = async (slug: string, key: string): Promise<Block> => {
  // const block = await fetchPageBlock(slug, key);

  // @NOTE: Mocking block for IMAGE
  const block: Block = {
    layout: BlockType.Image,
    key: 'i70sk9qeJ8v2I0gr',
    attributes: {
      src: 'https://images0.acc.persgroep.net/rcs/ORDWtxvZTDP1LCP9UPG-yYR3_OU/diocontent/902017766/_fitwidth/763?appId=93a17a8fd81db0de025c8abd1cca1279&quality=0.8',
      alt: 'test image',
      width: 389,
      height: 286,
      title: 'Een heel mooi plaatje vanuit Supernova',
    },
  };

  return await resolveBlockData(block);
};

export const resolveBlockData = async (block: Block): Promise<Block> => {
  const { layout, attributes } = block;

  switch (layout) {
    case BlockType.CategoryArticleList:
      const { category, limit } = attributes;

      if (category) {
        const article = await fetchCategoryArticles({ category, limit });
        return {
          ...block,
          data: article.results,
        };
      }
    case BlockType.Image:
      const { src = '', alt = '' } = attributes;

      return {
        ...block,
        data: {
          src,
          alt,
        },
      };
    case BlockType.Content:
      const { text = '' } = attributes;

      return {
        ...block,
        data: {
          text,
        },
      };
    default:
      console.warn(`No block config found for ${layout} type`);
  }
};

export const fetchPageWithBlocks = async (slug: string): Promise<PageWithBlocks> => {
  const page = (await fetchPage(slug)) as unknown as PageWithBlocks;

  const content = page
    ? await page.content.reduce(async (acc, block) => {
        const blocks = await acc;

        blocks.push(await resolveBlockData(block));

        return blocks;
      }, Promise.resolve([]) as Promise<Block[]>)
    : null;

  return {
    ...page,
    content,
  };
};
