export const Header: React.FC = ({ children }) => {
    return <header>{children}</header>;
  };
  