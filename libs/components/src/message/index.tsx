interface LoadingProps {
    message?: string;
    error?: boolean;
  }
  
  export const Message: React.FC<LoadingProps> = ({ message = '', error = false }) => {
    return <div className={`${error} ? 'text-red-500'`}>{message}</div>;
  };
  