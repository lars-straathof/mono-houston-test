export const Footer: React.FC = ({ children }) => {
    return <footer>{children}</footer>;
  };
  