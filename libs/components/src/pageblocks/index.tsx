import type { Block as BlockInterface, PageWithBlocks } from '@mono-houston/data-access';
import { ArticleListBlock, ImageBlock, ContentBlock } from '@mono-houston/components';

interface Props {
  page: PageWithBlocks;
}

export enum BlockType {
  CategoryArticleList = 'CATEGORY_ARTICLE_LIST',
  Image = 'IMAGE',
  Content = 'CONTENT',
}

export const SingleBlock = ({ block }: { block: BlockInterface }): React.ReactElement => {
  switch (block.layout) {
    case BlockType.CategoryArticleList:
      return <ArticleListBlock {...block} />;
    case BlockType.Image:
      return <ImageBlock {...block} />;
    case BlockType.Content:
      return <ContentBlock {...block} />;
    default:
      console.warn(`No block Component found for block ${block.layout}`);
      return <span></span>;
  }
};

export const PageBlocks: React.FC<Props> = ({ page }) => {
  if ((page.content && page.content.length === 0) || page.content === null) return null;

  return (
    <div className='pageBlocks'>
      {page.content.map((block) => {
        return (
          <div key={`block_${block.key}`}>
            <SingleBlock block={block} />
          </div>
        );
      })}
    </div>
  );
};
