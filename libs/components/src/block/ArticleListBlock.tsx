import { Article } from '@dpg-3c-jupiter/article-list-api';
import {
  ArticleCategory,
  ArticleList,
  generateDefaultImg,
  generatePictureSources,
  Picture,
} from '@dpg-3c-jupiter/ui';

import { Block } from '@mono-houston/data-access';

export const ArticleListBlock: React.FC<Block> = (block) => {
  const { category: slug } = block.attributes;

  return (
    <>
      <ArticleCategory slug={slug} />
      <ArticleList
        data={block.data as Article[]}
        PictureComponent={(article) => (
          <Picture
            sources={generatePictureSources(article.featuredImage, '128px')}
            defaultSource={generateDefaultImg(article.featuredImage)}
            classes={{ img: ['object-cover', 'w-32', 'h-24'] }}
            sizes='128px'
          />
        )}
      />
    </>
  );
};
