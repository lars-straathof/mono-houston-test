import { Block } from '@mono-houston/data-access';

// @TODO: implement a HTML sanitizer e.g.
// https://www.npmjs.com/package/sanitize-html
// https://atlassian.dpgmedia.net/jira/browse/HOUS-466
const sanitizeHtml = (input: string) => ({
  __html: input,
});

export const ContentBlock: React.FC<Block> = (block) => {
  const { text } = block.attributes;

  return <div dangerouslySetInnerHTML={sanitizeHtml(text)} />;
};
