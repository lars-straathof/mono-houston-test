import { Picture } from '@dpg-3c-jupiter/ui';
import { createUseStyles } from 'react-jss';
import { Block } from '@mono-houston/data-access';
import { ChevronRightIcon } from '@heroicons/react/solid';

const useStyles = createUseStyles({
  imageContainer: {
    position: 'relative',
  },
  title: {
    backgroundColor: 'black',
    opacity: 0.6,
    color: 'white',
    fontWeight: 'bold',
    position: 'absolute',
    width: '100%',
    padding: 10,
    bottom: 0,
  },
});

export const ImageBlock: React.FC<Block> = (block) => {
  const { src, alt, width, height, title } = block.attributes;

  const classes = useStyles();

  return (
    <div className={classes.imageContainer} style={{ width }}>
      <Picture defaultSource={{ src, width, height }} alt={alt} width={width} height={height} />
      <div className={`flex flex-row items-center ${classes.title}`}>
        <div className={`flex-grow line-clamp-3`}>{title}</div>
        <div className='flex-none w-5'>
          <ChevronRightIcon width={15} />
        </div>
      </div>
    </div>
  );
};
