import Head from 'next/head';

// Preload or prefetch or dns-prefetch items here to optimize performance
// Fonts are a good candiate and external asset domains

export const ResourceHints: React.FC = () => {
  return (
    <Head>
      {/* <link rel='preload' href='font.woff2' as='font' type='font/woff2' crossOrigin='anonymous' />
      <link rel='preconnect' href='https://media.dpgmediamagazines.nl' /> */}
    </Head>
  );
};
