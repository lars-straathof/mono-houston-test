import Head from 'next/head';

// Defines PWA linking and required (masked) Favicons

interface Props {
  themeColor: string;
}

export const Pwa: React.FC<Props> = (props) => {
  return (
    <Head>
      <meta name='theme-color' content={props.themeColor} />
      <link rel='manifest' href='/manifest.json' />
      <link rel='icon' href='/pwa/favicon_vt.png' type='image/png' sizes='192x192' />
      <link rel='apple-touch-icon' href='/pwa/favicon_vt.png' type='image/png' sizes='180x180' />
    </Head>
  );
};
