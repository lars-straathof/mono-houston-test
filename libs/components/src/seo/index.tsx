import Head from 'next/head';

interface Props {
  siteTitle: string;
  title: string;
  description: string;
  image: string;
  robots: string;
  url: string;
}

export const Seo: React.FC<Props> = (props) => {
  return (
    <Head>
      <title>{props.title ? props.title + ' - ' + props.siteTitle : props.siteTitle}</title>
      <meta name='canonical' content={props.url} />
      <meta name='robots' content={props.robots} />
      <meta name='description' content={props.description} />
      <meta property='og:type' content='website' />
      <meta property='og:title' content={props.title} />
      <meta property='og:description' content={props.description} />
      <meta property='og:url' content={props.url} />
      <meta property='og:locale' content='nl_NL' />
      <meta property='og:site_name' content={props.siteTitle} />
      <meta property='og:image' content={props.image} />
    </Head>
  );
};
