import { Article } from '@dpg-3c-jupiter/article-list-api';
import { generateDefaultImg, generatePictureSources, Picture } from '@dpg-3c-jupiter/ui';

const PRELOAD_ITEMS = 3;

export const ArticleListPicture = (article: Article, index: number): React.ReactElement => (
  <Picture
    sources={generatePictureSources(article.featuredImage, '128px')}
    defaultSource={generateDefaultImg(article.featuredImage)}
    priority={index < PRELOAD_ITEMS}
    classes={{ img: ['object-cover', 'w-32', 'h-24'] }}
    sizes='128px'
  />
);
