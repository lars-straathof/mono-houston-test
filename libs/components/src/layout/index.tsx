import { signIn, useSession } from 'next-auth/client';
import Link from 'next/link';

import { ConsentSettingsLink, ProfileIcon, Menu } from '@dpg-3c-jupiter/ui';
import { Gtm, GtmDataLayer } from '@dpg-3c-jupiter/common';

import { SearchIcon } from '@heroicons/react/solid';

import { Footer, Header, Pwa, ResourceHints, Seo } from '@mono-houston/components';
import { useRouter } from 'next/dist/client/router';
import { Menu as MenuProps } from '@mono-houston/data-access';

interface Props {
  siteTitle?: string;
  title?: string;
  description?: string;
  image?: string;
  robots?: string;
  url?: string;
  themeColor?: string;
  siteCountry?: string;
  dcParams?: string;
  dcKeywords?: string;
  articleKeywords?: string;
  pagePlatformType?: string;
  pageCategoryFull?: string;
  pageCategory?: string;
  pageSubCategory?: string;
  pageType?: string;
  articleId?: string;
  contentId?: string;
  menu?: MenuProps;
}

export const Layout: React.FC<Props> = ({
  siteTitle = 'Jupiter Base',
  title = 'Jupiter Base',
  description = 'Op Kekmama.nl vind je verhalen en tips over liefde &amp; relatie, opvoeden, de deur uit, vakantie, de leukste shop-items en meer. Met humor en relativering schrijven we over alles wat jou boeit als moeder.',
  image = '/favicons/favicon_masked_filled_1200.png',
  robots = 'all',
  url = 'https://www.kekmama.nl',
  themeColor = '#000000',
  siteCountry = 'NL',
  dcParams = '',
  dcKeywords = '',
  articleKeywords = '',
  pagePlatformType = '',
  pageCategoryFull = '',
  pageCategory = '',
  pageSubCategory = '',
  pageType = '',
  articleId = '',
  contentId = '',
  children,
  menu,
}) => {
  const [session] = useSession();
  const router = useRouter();
  return (
    <>
      <ResourceHints />
      <Pwa themeColor={themeColor} />
      <Seo
        siteTitle={siteTitle}
        title={title}
        description={description}
        image={image}
        robots={robots}
        url={url}
      />
      <GtmDataLayer
        siteTitle={siteTitle}
        siteCountry={siteCountry}
        siteLocation={process.env.NEXT_PUBLIC_APP_ENV}
        networkType={'internal'}
        siteCtsSlug={siteTitle.replace(/ /g, '_')}
        dcParams={dcParams}
        dcKeywords={dcKeywords}
        articleKeywords={articleKeywords}
        pagePlatformType='web'
        pageCategoryFull={pageCategoryFull}
        pageCategory={pageCategory}
        pageSubCategory={pageSubCategory}
        pageType={pageType}
        articleId={articleId}
        contentId={contentId}
      />
      <Gtm gtmId={process.env.NEXT_PUBLIC_GTM_ID} />
      <div className='grid grid-rows-header-main-footer min-h-screen'>
        <Header>
          <nav className='border-b border-black'>
            <div className='container mx-auto px-4 flex items-center flex-nowrap'>
              <div className='flex flex-grow'>
                <Menu data={menu} isOpen={false} />
              </div>

              <div className='flex flex-grow'>
                <Link href='/'>
                  <a className='py-4'>
                    <img
                      src='/img/logo_jp.png'
                      alt=''
                      className='w-28 md:w-40'
                      width='364'
                      height='80'
                    />
                  </a>
                </Link>
              </div>

              <div className='justify-self-end w-10'>
                <Link href='/search'>
                  <a className=''>
                    <SearchIcon className='h-6 w-6' />
                  </a>
                </Link>
              </div>

              <div className='justify-self-end'>
                {!session && (
                  <ProfileIcon
                    onClickLogin={() => signIn('pip')}
                    onClickProfile={() => {
                      console.log('onClickProfile not implemented');
                    }}
                    iconSize={30}
                  />
                )}

                {session && (
                  <ProfileIcon
                    picture='https://placeimg.com/100/100/people'
                    onClickLogin={() => {
                      console.log('onClickLogin not implemented');
                    }}
                    onClickProfile={() => {
                      router.push('/profile');
                    }}
                    userNick={session.user.email}
                    iconSize={30}
                  />
                )}
              </div>
            </div>
          </nav>
        </Header>

        <div className=''>{children}</div>

        <Footer>
          <div className='bg-black text-white text-xs'>
            <nav className='flex justify-center border-b border-black container mx-auto py-8'>
              © 2021 DPG Media Magazines B.V. &nbsp;-&nbsp;
              <ConsentSettingsLink>Cookie-instellingen</ConsentSettingsLink>
              {process.env.NEXT_PUBLIC_BUILD_NUMBER && (
                <span>
                  &nbsp;- Build {process.env.NEXT_PUBLIC_BUILD_NUMBER}@
                  {process.env.NEXT_PUBLIC_BUILD_COMMIT}
                </span>
              )}
            </nav>
          </div>
        </Footer>
      </div>
    </>
  );
};
