export const lightTheme = {
  ui: {
    backgroundColor: '#000',
    buttonTextColor: 'white',
  },
  primaryColor: 'gray',
};
